import React, {Fragment, useEffect, useState} from 'react';
import axios from 'axios';
import ArticlePreview from "../components/ArticlePreview";
import {API} from "../api"

const Articles = ({params}) => {

  const [articlePreviews, setArticlePreviews] = useState([]);
  
  const createArticle = (article) => (
      <Fragment key={article.id} >
        <ArticlePreview {... article} />
        <hr className="article-hr" />
      </Fragment>
  );

  useEffect(() => {
      if (params.params === undefined) {
          params.params = '';
      }

      axios.get(API.getAllArticles(params.params))
        .then(response => {
          let previews = response.data.map(createArticle);
          setArticlePreviews(previews);
        })
  }, [params]);

  return (
    <Fragment>
      {articlePreviews}
    </Fragment>
  )
};

export default Articles;