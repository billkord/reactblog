import React, {useEffect, useState} from 'react';
import axios from "axios";
import {API} from "../api";
import './RandomArticle.css'
import {useHistory} from "react-router-dom";

const RandomArticle = () => {

  const history = useHistory();
  const [article, setArticle] = useState({});

  const openArticle = () => {
    let articleUrl = `/article/${article.id}`
    history.push(articleUrl);
  };
  
  useEffect(() => {
    axios.get(API.getAllArticles(''))
      .then(response => {
        let articles = response.data;
        let min = Math.ceil(0);
        let max = Math.floor(articles.length);
        let i = Math.floor(Math.random() * (max - min + 1)) + min;
        setArticle(articles[i]);
      });
  }, []);
  
  return (
      <div>
        <div className="random-article-header">
          <h5>Random Post</h5>
          <hr style={{height:"2px",borderWidth:"0",color:"gray",backgroundColor:"#0060af"}} />
          <div className="random-article-body">
            <img className="img-fluid" alt={article.slug} src={article.cover_image} style={{cursor: 'pointer'}} onClick={openArticle}/>
            <p><strong> { article.title }</strong> </p>
          </div>
        </div>
      </div>
  );
};

export default RandomArticle;