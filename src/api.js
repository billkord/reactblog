import axios from "axios";

const API_BASE_URL = "https://dev.to/api";
const ARTICLES_PATH = "/articles";
const ARTICLES_TAG = "/?tag="

export const API = {
  getAllArticles: (tags) => API_BASE_URL + ARTICLES_PATH + ARTICLES_TAG + tags,
  getArticle: (id) => `${API_BASE_URL}${ARTICLES_PATH}/${id}`,
  getPopularArticle: () => axios.get(API.getAllArticles('')).then(articles => articles.data.sort((a,b) => b.positive_reactions_count - a.positive_reactions_count ).slice(0,3))
}
