import React, { useState } from "react";
import { FaRegCalendarAlt, FaRegComment, FaEye } from 'react-icons/fa';

const ArticlePreview = ({...article}) => {

  const [publishedDate] = useState(new Date(article.published_at));
  const [totalComments] = useState(article.comments_count);
  const [username] = useState(article.user.username);
  
  const displayPublishDate = () => {
    let date = new Date(publishedDate);
    if (publishedDate == null || isNaN(date.getTime())) {
      return ' - ';
    }
    const monthFormatter = new Intl.DateTimeFormat('en', { month: 'short' });
    return `${monthFormatter.format(date).toLocaleLowerCase()}/${date.getDate()}/${date.getFullYear()}`
  }

  // TODO find a better way to present this
  return (
    <div className="light-s-gray-text">
      {username} &nbsp; &nbsp;
      <FaRegCalendarAlt /> &nbsp; {displayPublishDate()} &nbsp; &nbsp;
      <FaRegComment />  &nbsp; {totalComments} &nbsp; &nbsp;
      <FaEye> </FaEye>
    </div>
  );
};

export default ArticlePreview;