import {Fragment, useState} from "react";
import Tag from "../Tag/Tag";

const HeaderItem = ({...props}) => {
    const [article] = useState(props.article);
    const myStyle = {
        width: "100%",
        height: 'auto'
    }
    return (
        <Fragment>
          <img alt={article.slug} src={article.cover_image} style={myStyle} />
          <div className="header-title-box">
            <Tag tags={article.tag_list} />
            <p className="header-title">{article.title}</p>
          </div>
        </Fragment>
    )
}
export default HeaderItem
