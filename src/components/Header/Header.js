import './Header.css'
import React, {useEffect, useState} from 'react';
import axios from "axios";
import {API} from "../../api";
import HeaderItem from "./HeaderItem";
import {Col, Row} from "react-bootstrap";


const  Header = () => {

    const [articles, setArticles] = useState([])

    async function fetchArticles(){
        const articlesResponse = await axios.get(API.getAllArticles(''))
        const data = articlesResponse.data;
        return [data[9], data[8], data[12]];
    }

    useEffect(() => {
        fetchArticles()
            .then(response => {
                let headerItems = response.map(article => 
                    <Col key={article.id + '_Col'} style={{padding: '0', margin: '0'}}>
                        <HeaderItem key={article.id} article={article}/>
                    </Col>
                );
                setArticles(headerItems);
            })
    }, []);

    return(
        <Row style={{paddingTop: '4.2rem'}}>
            {articles}
        </Row>
    );
}






export default Header
