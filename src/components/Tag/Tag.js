import "./Tag.css"
import {useEffect, useState} from "react";

const Tag = (props) => {
  
  const [tags, setTags] = useState([]);
  
  useEffect(() => {
    const showFirst = props.showFirst != null ? props.showFirst : false;
    const tmpTags = (props.tags || []).map(tag => (
        <div key={tag} className="btn-div">
          { tag }
        </div>
    ))
    setTags(showFirst ? tmpTags[0] : tmpTags);
  }, [props.tags, props.showFirst]);
  
  return (
      <div>
        { tags }
      </div>
  );
};

export default Tag;