import React, {useState} from "react";
import "./PopularPosts.css"
import ArticleInfo from "../ArticleInfo";
import {useHistory} from "react-router-dom";

export const PopularPostItem = ({...props}) => {

    const history = useHistory();
    const [post] = useState(props.post);

    const openArticle = () => {
      let articleUrl = `/article/${post.id}`
      history.push(articleUrl);
    };

    return (
        <div className="popular-posts-container">
            <img alt={post.slug} src={post.cover_image} className="img-fluid img-thumbnail img-container"  style={{cursor: 'pointer'}} onClick={openArticle}/>
            <div className="popular-posts-container-info">
                <p>{post.title}</p>
                <ArticleInfo {...post} />
            </div>

        </div>
    )
}
