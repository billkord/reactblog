import {useEffect, useState} from "react";
import {API as Api} from "../../api";
import './PopularPosts.css'
import {PopularPostItem} from "./PopularPostItem";

const PopularPosts = () => {
    
    const [popularPosts, setPopularPosts] = useState([]);

    useEffect(() => {
        Api.getPopularArticle()
            .then(response => {
                setPopularPosts(response)
            })
    },[])
    
    return (
        <div className="posts-box">
            <div className="posts-box-header">
                <h5>Popular Posts</h5>
                <hr style={{height:"2px",borderWidth:"0",color:"gray",backgroundColor:"#0060af"}} />
            </div>
          { popularPosts.map(post => <PopularPostItem key={post.id} post={post}/>) }
        </div>
    )
}

export default PopularPosts
