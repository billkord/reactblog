import React from "react";

var style = {
    color: "#FFFFFF",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    left: "0",
    bottom: "0",
    right: "0",
    height: "60px",
    width: "100%",

}

var phantom = {
    display: 'block',
    padding: '20px',
    height: '60px',
    width: '100%',
}

const Footer = ({ children }) => {
    return (
        <div>
            <div style={phantom} />
            <div className="deep-blue-background" style={style}>
                { children }
            </div>
        </div>
    )
}

export default Footer;