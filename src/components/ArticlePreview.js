import React, {Fragment} from 'react';
import { useHistory } from "react-router-dom";
import {Row, Col} from 'react-bootstrap';
import { FiChevronRight } from 'react-icons/fi';
import ArticleInfo from './ArticleInfo';
import Tag from "./Tag/Tag";


const ArticlePreview = ({...article}) => {

  const history = useHistory();

  const openArticle = () => {
    let articleUrl = `/article/${article.id}`
    history.push(articleUrl);
  };

  return (
    <Fragment>
      <Row className="justify-content-center">
        <img alt={article.slug} src={article.cover_image} className="img-fluid" />
      </Row>
      <Row className="justify-content-center">
        <Tag tags={article.tag_list}/>
        {/*<Button className="deep-blue-background"> Product </Button>*/}
      </Row>
      <Row>
        <Col style={{textAlign: "center"}}>
          <h4> { article.title } </h4>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <ArticleInfo {...article} />
      </Row>
      <Row className="justify-content-center">
        <p className="light-m-gray-text"> { article.description } </p>
      </Row>
      <Row className="justify-content-end">
        <div style={{cursor: "pointer", color: "#0088d7"}} onClick={openArticle}>
          READ MORE <FiChevronRight size={19} style={{marginBottom: "2px"}} />
        </div>
      </Row>
    </Fragment>
  );
};

export default ArticlePreview;