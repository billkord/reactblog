import {
    AppBar,
    Drawer,
    IconButton,
    makeStyles,
    Toolbar,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React, {useEffect, useState} from "react";
import {NavLink, useHistory, withRouter} from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles(() => ({
    header: {
        backgroundColor: "white",
        paddingRight: "79px",
        paddingLeft: "118px",
        "@media (max-width: 900px)": {
            paddingLeft: 0,
        },
    },
    logo: {
        fontFamily: "Work Sans, sans-serif",
        fontWeight: 600,
        color: "black",
        textAlign: "left",
    },
    img: {
        width: '8%',
        height: 'auto'
    },
    menuButton: {
        color: 'grey',
        fontFamily: "Open Sans, sans-serif",
        fontWeight: 700,
        size: "18px",
        marginLeft: "38px",
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
    },
    drawerContainer: {
        padding: "20px 30px",
    },
    searchBar: {
        marginBottom: '18px'
    },
    searchIcon: {
        color: 'black',
        marginTop: "25px",

    },
    active: {
        borderTop: '5px solid #0088d7',
        textDecoration: "none"
    }
}));

const Header = () => {
    
    const { header, img, menuButton, toolbar, drawerContainer, searchIcon, searchBar, active } = useStyles();
    let history = useHistory();
    
    const [state, setState] = useState({
        mobileView: false,
        drawerOpen: false,
    });
    
    const handleChange = (e) =>{
        history.push(`/search/${e.target.value}`);
    }
    
    const { mobileView, drawerOpen } = state;

    useEffect(() => {
        const setResponsiveness = () => {
            return window.innerWidth < 900
                ? setState((prevState) => ({ ...prevState, mobileView: true }))
                : setState((prevState) => ({ ...prevState, mobileView: false }));
        };

        setResponsiveness();

        window.addEventListener("resize", () => setResponsiveness());

        return () => {
            window.removeEventListener("resize", () => setResponsiveness());
        };
    }, []);

    const displayDesktop = () => {
        return (
            <Toolbar className={toolbar}>
                <img src='https://www.intrasoft-intl.com/themes/custom/isatheme/logo.png' alt="Logo" className={img} />;
                <div>{getMenuButtons()}</div>
                <div className={searchBar}>
                    <SearchIcon className={searchIcon}/>
                    <TextField onBlur={handleChange} id="standard-basic" label="Search" />
                </div>
            </Toolbar>
        );
    };

    const displayMobile = () => {
        const handleDrawerOpen = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: true }));
        const handleDrawerClose = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: false }));

        return (
            <Toolbar>
                <IconButton
                    {...{
                        edge: "start",
                        color: "blue",
                        "aria-label": "menu",
                        "aria-haspopup": "true",
                        onClick: handleDrawerOpen,
                    }}
                >
                    <MenuIcon />
                </IconButton>

                <Drawer
                    {...{
                        anchor: "left",
                        open: drawerOpen,
                        onClose: handleDrawerClose,
                    }}
                >
                    <div className={drawerContainer}>{getDrawerChoices()}

                        <SearchIcon className={searchIcon}/>
                        <TextField onBlur={handleChange} id="standard-basic" label="Search" />
                    </div>
                </Drawer>

                <div>{intraLogo}</div>
            </Toolbar>
        );
    };

    const getDrawerChoices = () => {

            return (
               <div >
                   <li ><NavLink to="/home"  activeClassName={active}>Home</NavLink></li>
                   <li><NavLink to="/search/lifestyle"  activeClassName={active}>Lifestyle</NavLink></li>
                   <li><NavLink to="/search/travel"  activeClassName={active}>Travel</NavLink></li>
               </div>

            );

    };

    const intraLogo = (
        <img src='https://www.intrasoft-intl.com/themes/custom/isatheme/logo.png' alt="Logo" className="w-25" />
    );

    const getMenuButtons = () => {

            return (
<div className={menuButton}>
                <NavLink to="/home" className={menuButton} activeClassName={active}>Home</NavLink>
                <NavLink to="/search/lifestyle" className={menuButton} activeClassName={active}>Lifestyle</NavLink>
                <NavLink to="/search/travel" className={menuButton} activeClassName={active}>Travel</NavLink>

</div>
            );

    };

    return (
        <header>
            <AppBar className={header}>
                {mobileView ? displayMobile() : displayDesktop()}
            </AppBar>
        </header>
    );
}
export default withRouter(Header);