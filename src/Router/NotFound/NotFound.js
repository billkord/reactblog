import React from 'react';

const NotFound = ({location}) => (<h2>{`The path ${location.pathname} not found`}</h2>);

export default NotFound;