import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Home from "./Home/Home";
import Footer from "../components/Footer";
import NavBar from "../components/NavBar/NavBar";
import NotFound from "./NotFound/NotFound";
import ArticleDetails from "./ArticleDetails/ArticleDetails";

const RouterExample = () => (
  <BrowserRouter>
    <div>
      <NavBar />
      <Switch>
        <Route exact path="/home" render={Home} />
        <Route path="/search/:params" render={Home} />
        <Route path="/article/:id" component={ArticleDetails} />
        <Redirect from="/" to="/home" />
        <Route component={NotFound} />
      </Switch>
      <Footer> George, Konstantinos, Vasilis </Footer>
    </div>
  </BrowserRouter>
);

export default RouterExample;
