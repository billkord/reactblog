import React, { Fragment } from 'react';
import Articles from '../../Articles/Articles';
import Scaffold from '../Scaffold';
import Header from "../../components/Header/Header";

const Home = ({match:{params}, location, history}) => (
    <Fragment>
      <Header/>
      <Scaffold>
        <Articles params={params} type="home" />
      </Scaffold>
    </Fragment>
);

export default Home;
