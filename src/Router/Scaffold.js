import React, {Fragment} from "react";
import PopularPosts from "../components/RelatedPosts/PopularPosts";
import RandomArticle from "../Articles/RandomArticle";
import './Scaffold.css'
import {Col, Row} from "react-bootstrap";

const Scaffold = (props) => (
    <Fragment>
      <Row className="justify-content-center main-container">
        <Col xs={8} style={{textAlign: "center"}}>
          { props.children }
        </Col>

        <Col xs={4} style={{textAlign: "center", paddingLeft: '3rem'}}>
          <div>
            <PopularPosts/>
          </div>

          <div>
            <RandomArticle />
          </div>
        </Col>
      </Row>

    </Fragment>
);

export default Scaffold;