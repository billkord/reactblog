import React, {useState} from "react";
import './ArticleDetails.css';
import {Row} from "react-bootstrap";

const ArticleHeader = ({...props}) => {
    
    const [article] = useState(props.article)

    const myStyle = {
        backgroundImage: `url(${article.social_image})`,
        backgroundSize: "cover",
        width: "100%"
    }

    return (
        <div>
            <Row style ={{height:'400px'}}>
                <div style={myStyle} className="header-item">
                    <div className="header-title-box">
                        <p className="header-title">{article.title}</p>
                    </div>
                </div>
            </Row>
        </div>

    )
}
export default ArticleHeader
