import React, {Fragment, useEffect, useState} from "react";
import Scaffold from "../Scaffold";
import ArticlePreview from "./ArticlePreview";
import ArticleHeader from "./ArticleHeader";
import axios from "axios";
import {API} from "../../api";

const ArticleDetails = ({match:{params}, location, history}) => {

    const [article, setArticle] = useState()

    useEffect(() => {
        const { id } = params;
        axios.get(API.getArticle(id))
            .then(response => {
                const articleResponse = response.data;
                const content = <Fragment>
                    <ArticleHeader article = {articleResponse}/>
                    <Scaffold >
                        <ArticlePreview article = {articleResponse}/>
                    </Scaffold>
                </Fragment>

                setArticle(content);
            })
    }, [params]);

   return (
       <Fragment>
           {article}
       </Fragment>


    );
}

export default ArticleDetails
