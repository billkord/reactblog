import React, {Fragment, useState} from "react";
import ArticleInfo from "../../components/ArticleInfo";

const ArticlePreview = ({...props}) => {

    const [article] = useState(props.article);

    return (
        <Fragment>
            <img alt={article.slug} src={article.cover_image} className="img-fluid"/>
            <h5>{article.title}</h5>
            <ArticleInfo {...article} />
            <div
                dangerouslySetInnerHTML={{
                    __html: article.body_html
                }}>
            </div>
        </Fragment>

    );
}
export default ArticlePreview
