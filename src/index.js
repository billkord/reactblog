import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {} from './style.css';

import Router from './Router/Router';

ReactDOM.render(<Router />, document.getElementById('root'));
